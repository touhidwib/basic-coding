//
//  main.cpp
//  BasicCode
//
//  Created by WIB on 5/1/21.
//  Copyright © 2021 WIB. All rights reserved.
//

#include <iostream>
//#include <stdio.h>
using namespace std;

int main(int argc, const char * argv[]) {
    // insert code here...
    //cout << "Hello, World!"<< endl;
    
    /*
    int i = 5000;
    float f = 30.50;
    double d = 666060.77;
    char c = 'C';
    
    cout << i << endl;
    cout << f << endl;
    cout << d << endl;
    //cout << "The character is ";
    cout << "Character " <<  c << endl;
     */
    
    /*
    // Getting INPUT
    int ii;
    float ff;
    double dd;
    char cc;
    
    cout << "Enter integer value here: ";
    cin >> ii;
    cout << "The integer you entered is " << ii << endl;
    
    cout << "Enter the floating value here: ";
    cin >> ff ;
    cout << "The floating value you entered is " << ff << endl;
    
    cout << "Enter the douable floating point value here: ";
    cin >> dd;
    cout << "The double value you entered is " << dd << endl;
    
    cout << "Enter the character here: ";
    cin >> cc;
    cout << "The character is " << cc << endl;
     
    
    // Getting all the input at once
    
    cout << "Enter your values by following order, INT, FLOAT, DOUBLE, CHARACTER: ";
    cin >> ii >> ff >> dd >> cc;
    
    cout << "Interger " << ii << endl;
    cout << "Float " << ff << endl;
    cout << "Double " << dd << endl;
    cout << "Chaaracter " << cc << endl;
    */
    
    // Type conversion
    // Converting integer to double
    /*
    int num;
    double dNum;
    
    cout << "Enter the integer to be converted to double: ";
    cin >> num;
    dNum = num;
    cout << "The double is " << dNum << endl;
    */
     
    // Converting double to integer
    /*
    int nnum;
    double ndNum;
    
    cout << "Enter the double to be converted to integer: ";
    cin >> ndNum;
    nnum = ndNum;
    cout << "The integer is " << nnum << endl;
     */
    
    // Arithmetic operators
    int a;
    int b;
    
    cout << "Enter the value of A: ";
    cin >> a;
    
    cout << "Enter the value of B: ";
    cin >> b;
    
    // Addition
    cout << "Addition: " << a+b << endl;
    
    // Subtraction
    cout << "Subtraction: " << a-b << endl;
    
    // Multiplication
    cout << "Multiplication: " << a*b << endl;
    
    // Division
    cout << "Division: " << a/b << endl;
    
    // Modulo
    cout << "Modulo: " << a%b << endl;
    cout << "Modulo: " << a%b << endl;
    
    
    
    return 0;
}


